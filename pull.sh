branchname=$(git symbolic-ref --short -q HEAD)

if [ $branchname != 'master' ]  ##cloudinary
then
	echo "Please run this script in master"
	exit
fi
 
echo "start..."
filename="$(date +%Y%m%d-%H%M%S.log)"
git pull origin master
sleep 1
git diff-tree -r --name-only --no-commit-id ORIG_HEAD HEAD |grep -E "static/src/images|static/src/banner|static/src/mail" > /d/$filename
if [[ -s /d/$filename ]] 
then
	echo "Sync Images to Cloundinary ..."
	php wwwroot/crons/local/sync_cloudinary.php --list=/d/$filename  --rootpath=$(pwd)/
fi

echo “Detect conflicts in the src directory”
grep -irE "<<<<<<<" --exclude "*.jpg" static/

